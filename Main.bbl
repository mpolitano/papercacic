\begin{thebibliography}{10}
\providecommand{\url}[1]{\texttt{#1}}
\providecommand{\urlprefix}{URL }
\providecommand{\doi}[1]{https://doi.org/#1}

\bibitem{Abad:2013}
Abad, P., Aguirre, N., Bengolea, V.S., Ciolek, D., Frias, M.F., Galeotti, J.P.,
  Maibaum, T., Moscato, M.M., Rosner, N., Vissani, I.: Improving test
  generation under rich contracts by tight bounds and incremental {SAT}
  solving. In: Sixth {IEEE} International Conference on Software Testing,
  Verification and Validation, {ICST} 2013, Luxembourg, Luxembourg, March
  18-22, 2013. pp. 21--30 (2013)

\bibitem{DBLP:conf/icse/BraioneDMP18}
Braione, P., Denaro, G., Mattavelli, A., Pezz{\`{e}}, M.: {SUSHI:} a test
  generator for programs with complex structured inputs. In: Proceedings of the
  40th International Conference on Software Engineering: Companion
  Proceeedings, {ICSE} 2018, Gothenburg, Sweden, May 27 - June 03, 2018. pp.
  21--24. {ACM} (2018)

\bibitem{Calcagno:2011}
Calcagno, C., Distefano, D., O'Hearn, P.W., Yang, H.: Compositional shape
  analysis by means of bi-abduction. J. ACM  \textbf{58}(6),  26:1--26:66 (Dec
  2011)

\bibitem{Clarke:2004}
Clarke, E., Kroening, D., Lerda, F.: A tool for checking {ANSI-C} programs. In:
  Tools and Algorithms for the Construction and Analysis of Systems (TACAS
  2004). Lecture Notes in Computer Science, vol.~2988, pp. 168--176. Springer
  (2004)

\bibitem{Fraser:2011}
Fraser, G., Arcuri, A.: Evosuite: Automatic test suite generation for
  object-oriented software. In: Proceedings of the 19th ACM SIGSOFT Symposium
  and the 13th European Conference on Foundations of Software Engineering. pp.
  416--419. ESEC/FSE '11, ACM, New York, NY, USA (2011)

\bibitem{Goldberg:1989}
Goldberg, D.E.: Genetic Algorithms in Search, Optimization and Machine
  Learning. Addison-Wesley Longman Publishing Co., Inc., Boston, MA, USA, 1st
  edn. (1989)

\bibitem{Pacheco:2007}
Pacheco, C., Ernst, M.D.: Randoop: Feedback-directed random testing for java.
  In: Companion to the 22Nd ACM SIGPLAN Conference on Object-oriented
  Programming Systems and Applications Companion. pp. 815--816. OOPSLA '07,
  ACM, New York, NY, USA (2007)

\bibitem{PonzioDrivers19}
Ponzio, P., Bengolea, V.S., Politano, M., Aguirre, N., Frias, M.F.:
  Automatically identifying sufficient object builders from module apis. In:
  Fundamental Approaches to Software Engineering - 22nd International
  Conference, {FASE} 2019, Held as Part of the European Joint Conferences on
  Theory and Practice of Software, {ETAPS} 2019, Prague, Czech Republic, April
  6-11, 2019, Proceedings (2019)

\bibitem{Pasareanu:2010}
P\u{a}s\u{a}reanu, C.S., Rungta, N.: Symbolic pathfinder: Symbolic execution of
  java bytecode. In: Proceedings of the IEEE/ACM International Conference on
  Automated Software Engineering. pp. 179--180. ASE '10, ACM, New York, NY, USA
  (2010)

\bibitem{Russell:2009}
Russell, S., Norvig, P.: Artificial Intelligence: A Modern Approach. Prentice
  Hall Press, Upper Saddle River, NJ, USA, 3rd edn. (2009)

\bibitem{Visser:2005}
Visser, W., Mehlitz, P.: Model checking programs with java pathfinder. In:
  Proceedings of the 12th International Conference on Model Checking Software.
  pp. 27--27. SPIN'05, Springer-Verlag, Berlin, Heidelberg (2005)

\bibitem{apache}
Website of the {Apache Collections} library.
  \url{https://commons.apache.org/proper/commons-collections/}

\bibitem{jgap}
Website of the {Java Genetic Algorithms Package}.
  \url{http://jgap.sourceforge.net}

\end{thebibliography}
