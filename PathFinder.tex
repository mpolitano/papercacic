\section{Construcción de drivers eficientes usando \emph{builders}} \label{sec:pathfinder}

Como se discutió anteriormente, Java PathFinder (JPF) \cite{Visser:2005} es una herramienta de verificación automática de programas, que permite implementar diversos tipos análisis. En este trabajo la utilizaremos para realizar (bounded) model checking de software, es decir, para explorar todas las posibles ejecuciones de un programa con entradas de tamaño acotado. Las propiedades a verificar en JPF pueden especificarse usando la construcción \texttt{assert} de Java. \texttt{assert} evalúa una condición lógica y produce una falla en el programa en caso de que esta sea falsa en la ejecución corriente. En caso de que la condición sea verdadera, \texttt{assert} no tiene ningún efecto sobre la ejecución. Por ejemplo, el siguiente código especifica la propiedad de que para cualquier lista de entrada \texttt{t}, insertar al inicio un valor entero \texttt{v} (\texttt{addFirst(v)}, con \texttt{v} entre 0 y \texttt{b}, con \texttt{b} el límite máximo en la cantidad de enteros a analizar, ver más adelante) que no pertenece a la lista (ver explicación de \texttt{Verify.ignoreIf} abajo), y luego eliminar el primer elemento de la lista (el valor agregado recientemente, ejecutando \texttt{removeFirst}), resulta en una lista con una cantidad de elementos igual a la original (antes de ejecutar \texttt{addFirst} y \texttt{removeFirst}; notar que la cantidad de elementos previa se almacena en \texttt{oldSize} al inicio). 

\begin{algorithm}[H]
\caption{Propiedad a chequear}
\label{alg:propiedad}
\begin{algorithmic}
{\fontsize{8pt}{8pt}\selectfont
\STATE $oldSize \gets $t.size()
\STATE $value \gets $Verify.getInt(0, b)
\STATE Verify.ignoreIf(t.contains(v))
\STATE t.addFirst(value)
\STATE t.removeFirst()
\STATE assert $oldSize = t.size()$ : "different size";
}
\end{algorithmic}
\end{algorithm}

Este código utiliza dos directivas muy importantes de JPF:

%Atraviesa sistemáticamente posibles rutas de ejecución dentro de un modelo (en este caso,los modelos son programa basado en Java) para exponer violaciones de propiedades tales como puntos muertos,excepciones no controladas, entre otras. Además, también informa de un seguimiento detallado de las ejecuciones que conduce a defectos o violaciones. 
%Se define un programa en particular como un árbol, lo que JPF realiza luego de la verificación se considera análogo a un proceso de búsqueda de profundidad. Aquí, el mecanismo de retroceso es muy importante ya que puede ver si hay rutas que aún no se han explorado, por lo que JPF teóricamente puede rastrear cualquier ruta posible dentro del programa basado en Java cuando se le da suficiente tiempo y uso de memoria.
%JPF proporciona estretegias para configurar la busqueda y evitar la explosion de estados, entre las mas conocidas y las que nosotros utilazaremos en este trabajo son:
\begin{itemize}
	\item %Poda de búsqueda: Restringir explícitamente la búsqueda es útil para propiedades altamente específicas de la aplicación, donde es obvio que ciertos valores no son de interés con respecto a la propiedad. Para realizar esto, JPF brinda la siguiente instruccion:
	%\begin{lstlisting}[language = Java, label=fig:propiedad, captionpos=b, basicstyle=\scriptsize]
        \texttt{Verify.ignoreIf(condicion)}:
	%\end{lstlisting}
        Evita explorar ejecuciones cuyo estado no cumplen con el predicado \texttt{condicion}. En el programa anterior, permite ignorar todas las ejecuciones en las que la lista contiene el valor \texttt{v} a insertar. Esto es similar a agregar una precondición a la propiedad a verificar.
	\item %Generadores de selecciones: La idea es obtener valores de entrada no-deterministas de JPF de manera que pueda analizar sistemáticamente todas las opciones relevantes. En su forma más simple, esto se puede usar como:
	%\begin{lstlisting}[language = Java, label=fig:propiedad, captionpos=b, basicstyle=\scriptsize]
        \texttt{int i = Verify.getInt(min,max)}: 
	%\end{lstlisting}
        Esta construcción de JPF explora todas las posibles ejecuciones del programa que resultan de asignar a \texttt{i} cualquier valor entre \texttt{min} y \texttt{max}. Esto significa que JPF ejecutará el código luego de esta instrucción con \texttt{i=min}, con \texttt{i=min+1}, ..., \texttt{i=max}. Esto introduce no determinismo, ya que en principio no hay garantías del orden en que se asignarán valores a \texttt{i} (aunque JPF tiene opciones para elejir un orden de ejecución particular \cite{}). En nuestro código previo, esto permite explorar todas las ejecuciones de nuestra propiedad con valores de \texttt{v} entre 0 y \texttt{b}. 
%	Esta instruccion valua todos los valores entre \emph{min} y \emph{max}.
\end{itemize}

Para poder realizar el análisis de esta propiedad, es necesario proveer a JPF los mecanismos necesarios para generar las listas de entrada para el parámetro \texttt{t}. Esto es necesario para cualquier tipo estructurado; notar que los valores enteros pueden generarse fácilmente con \texttt{Verify.getInt}. Nuestro objetivo es construir todas las listas posibles de tamaño máximo \texttt{b}, que almacenan elementos enteros entre \texttt{0} y \texttt{b} (verificación exhaustiva acotada). Notar que \texttt{b} es un parámetro del algoritmo, dado por el usuario. La generación exhaustiva acotada de estructuras para el parámetro \texttt{t} se implementa en el \emph{driver} del algortimo \ref{alg:driverBLD}. Al inicio, el \emph{driver} selecciona la cantidad de métodos a ejecutar, \texttt{nExec}, un número entre 0 y \texttt{b}. Como los métodos considerados agregan a lo sumo un elemento a la lista, ejecutar un máximo de \texttt{b} métodos resulta en listas de tamaño a lo sumo \texttt{b} (ejemplo, ejecutar \texttt{b} veces \texttt{addFirst}). Cada iteración del ciclo corresponde a la ejecución de un único método, seleccionado también de manera no determinísitica entre todos los disponibles. En el caso en que el usuario no conoce el conjunto de \emph{builders} (y no desea realizar el difícil trabajo de seleeccionarlos manualmente), la solución más segura para evitar descartar métodos importantes es utilizar todos los métodos disponibles en el módulo, como se muestra en el algortimo \ref{alg:driverAll}. En el cuerpo del ciclo cada método tiene asignado un único entero, y se elige no determinísiticamente un entero entre 0 y la cantidad de métodos para seleccionar el método a ejecutar la iteracción corriente. Por ejemplo, si \texttt{methodN=3} se ejecuta el método \texttt{add}. Es fácil ver que la cantidad de ejecuciones posibles a explorar por JPF crece exponencialmente con la cantidad de métodos disponibles (por cada método que se ejecuta en una iteración, hay \texttt{m} posibles métodos para ejecutar en la iteración siguiente). 

Si bien evitar este crecimiento exponencial no es posible en muchos casos, en este trabajo se propone utilizar sólo los builders detectados por el algoritmo genético explicado anteriormente para construir \emph{drivers}, para acelerar la verificación de propiedades en JPF y mejorar su escalabilidad en casos que ocurren típicamente en la práctica (como el caso de NodeCachingLinkedList presentado aquí). Como se muestra en la figura \ref{fig:driver}, sólo 3 métodos conforman un conjunto suficiente y minimal para la construcción de \texttt{drivers} para NodeCachingLinkedList. Utilizando sólo esos métodos, obtenemos el \emph{driver} del algoritmo \ref{alg:driverBLD}, que genera exactamente los mismos objetos de NodeCachingLinkedList que el anterior (porque los builders son suficientes y minimales), y por lo tanto JPF explora las mismas ejecuciones de la propiedad con ambos \emph{drivers}. Sin embargo, como se discute en la sección que sigue, eliminar métodos innecesarios del \emph{driver} produce ganancias sustanciales en tiempo y escalabilidad en el análisis de nuestra propiedad de ejemplo usando JPF. 



%En la figura \ref{alg:driverAll} se puede observar el uso de estas dos instrucciones. En primer lugar, de manera no-determinista,el metodo \emph{getInt} del paquete \emph{Verify} de JPF, elige entre 0 y \emph{bound}. Esta variable es definida por el usuario y es necesaria para acotar el espacio de busqueda. 



\begin{minipage}[t]{6cm}%6cm <==========================================

\begin{algorithm}[H]
\caption{Driver API}
\label{alg:driverAll}
\begin{algorithmic}
{\fontsize{8pt}{8pt}\selectfont
\STATE $t\gets $ new NLC()
\STATE $nExec\gets $ Verify.getInt(0, bound)
\FOR{$i=0...nExec$} 
	\STATE $methodN \gets $ Verify.getInt(0, nMet -1);
	\SWITCH{ methodN }
		\CASE {0}
				\STATE t.clear()
		\ENDCASE
		\CASE {1}
				\STATE t.removeFirst();		
		\ENDCASE
		\CASE {2}
				\STATE t.addLast(Obj)
		\ENDCASE
		\CASE {3}
				\STATE t.add(t)
		\ENDCASE

		...

		\CASE {27}
				\STATE t.getLast()
		\ENDCASE
		\CASE {28}
				\STATE t.get(Verify.getInt(0, bound))
		\ENDCASE
		\CASE {29}
				\STATE t.set(Verify.getInt(0, bound),Obj)
		\ENDCASE
	\ENDSWITCH
\ENDFOR
}
\end{algorithmic}
\end{algorithm}
\end{minipage}%
\vspace{1cm}
\begin{minipage}[t]{6cm}%6cm <==========================================
\begin{algorithm}[H]
\caption{Driver con Herramienta}
\label{alg:driverBLD}
\begin{algorithmic}
{\fontsize{8pt}{8pt}\selectfont
\STATE $t\gets $ new NLC()
\STATE $nExec\gets $ Verify.getInt(0, bound)
\FOR{$i=0...nExec$} 
	\STATE $methodN \gets $ Verify.getInt(0, nMet -1);
	\SWITCH{ methodN }
		\CASE {0}
				\STATE t.addFirst(Obj)
		\ENDCASE
		\CASE {1}
				\STATE t.removeFirst()
		\ENDCASE
	\ENDSWITCH
\ENDFOR
}
\end{algorithmic}
\end{algorithm}
\end{minipage}%
\vspace{-1cm}